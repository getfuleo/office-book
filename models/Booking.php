<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "booking".
 *
 * @property int $id
 * @property int|null $employee_id
 * @property int|null $place_id
 * @property string|null $date_created
 * @property string|null $date_updated
 * @property string|null $date_start
 * @property string|null $date_end
 *
 * @property Employee $employee
 * @property Place $place
 */
class Booking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'booking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'place_id', 'date_start', 'date_end'], 'required'],
            [['employee_id', 'place_id'], 'integer'],
            [['date_created', 'date_updated', 'date_start', 'date_end'], 'safe'],
            [['id'], 'unique'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['place_id'], 'exist', 'skipOnError' => true, 'targetClass' => Place::className(), 'targetAttribute' => ['place_id' => 'id']],
            ['date_start', 'compare', 'compareAttribute'=> 'date_end', 'operator' => '<', 'enableClientValidation' =>true]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Pracownik',
            'place_id' => 'Miejsce',
            'date_created' => 'Data utworzenia',
            'date_updated' => 'Data aktualizacji',
            'date_start' => 'Rezerwacja od',
            'date_end' => 'Rezerwacja do',
        ];
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[Place]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Place::className(), ['id' => 'place_id']);
    }
    public function checkExistValidate($attribute, $params)
    {
        // var_dump($this->date_start).exit;
        $oBooking = self::find()
            ->where(
                [
                    'place_id' => $this->place_id,
                ]
            )
            ->andWhere([
                'AND',
                ['>=', 'date_start', $this->{$attribute}],
                ['<', 'date_end', $this->{$attribute}],
            ])
        // var_dump($oBooking->createCommand()->getRawSql()).exit;
            ->count();

        if ($oBooking) {
            $this->addError($attribute, 'Data jest zajęta');
        }

    }

    /**
     * Check function exists for booking
     *
     *  @property int $employee_id
     *  @property int $place_id
     *  @property string $date_start
     *  @property string $date_end
     *
     *  @return integer count
     */
    public static function checkExist(int $place_id, string $date_start, string $date_end): int
    {
        $oBooking = self::find()
            ->where(
                [
                    'place_id' => $place_id,
                ]
            )
            ->andWhere([
                'OR',
                ['between', 'date_start', $date_start, $date_end],
                ['between', 'date_end', $date_start, $date_end],
            ])
            ->count();

        return $oBooking;
    }
}
