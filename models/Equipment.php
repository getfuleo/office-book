<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipment".
 *
 * @property int $id
 * @property int|null $place_id
 * @property string|null $kind
 * @property string|null $model
 * @property string|null $name
 * @property string|null $date_buy
 * @property int|null $value
 * @property string|null $description
 *
 * @property Place $place
 */
class Equipment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['place_id', 'value'], 'integer'],
            [['description'], 'string'],
            [['kind', 'model', 'name', 'date_buy'], 'string', 'max' => 255],
            [['place_id'], 'exist', 'skipOnError' => true, 'targetClass' => Place::className(), 'targetAttribute' => ['place_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'place_id' => 'Miejsce',
            'kind' => 'Rodzaj',
            'model' => 'Model',
            'name' => 'Nazwa',
            'date_buy' => 'Data zakupu',
            'value' => 'Wartość',
            'description' => 'Opis',
        ];
    }

    /**
     * Gets query for [[Place]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Place::className(), ['id' => 'place_id']);
    }
}
