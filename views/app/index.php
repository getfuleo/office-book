<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */

$this->title = 'index';
?>
<div class="jumbotron">
    <h1 class="display-4">office.book</h1>
    <p class="lead">Aplikacja do rezerwacji miejsc pracy w biurze.</p>
    <hr class="my-4">

    <?php if (Yii::$app->user->isGuest) {?>
        <p>Zaloguj się, aby skorzystać z aplikacji.</p>
        <?=Html::a('Zaloguj się', 'app/login', ['class' => ' btn btn-sm btn-dark'])?>
    <?php }?>
</div>