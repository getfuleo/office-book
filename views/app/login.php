<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Zaloguj się';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="d-flex  justify-content-center">

        <div class="col-12 col-lg-4 ">

            <p class="alert alert-info">
                Możesz zalogować się na <strong>admin/admin</strong>
            </p>
            <div class="card">

                <div class="card-body">
                    <h5 class="card-title"><?= Html::encode($this->title) ?></h5>




                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        // 'layout' => 'horizontal',
                        // 'fieldConfig' => [
                        //     'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                        //     'labelOptions' => ['class' => 'col-lg-1 control-label'],
                        // ],
                    ]); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <?= $form->field($model, 'rememberMe')->checkbox([
                        // 'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    ]) ?>


                    <?= Html::submitButton('Zaloguj się', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>


                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>