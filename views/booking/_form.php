<?php

use app\models\Employee;
use app\models\Place;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Booking */
/* @var $form yii\widgets\ActiveForm */
?>
<p>
    <?=Html::a('&laquo; Wróć do listy', ['index'], ['class' => 'btn btn-light'])?>
</p>

<?php if (Yii::$app->session->hasFlash('bookingError')){ ?>

<div class="alert alert-danger">
    Podana data jest zajęta.
</div>

<?php } ?>
<div class="booking-form">
    <?php $form = ActiveForm::begin();?>
    <div class="row">
        <div class="col-12 col-md-4">
            <?=$form->field($model, 'employee_id')->dropDownList(
    ArrayHelper::map(
        Employee::find()->all(),
        'id',
        function ($data) {

            return $data['firstname'] . ' ' . $data['lastname'] . '(' . $data['email'] . ')';
        }
    ),
    ["prompt" => 'Wybierz pracownika']
);?>
        </div>
        <div class="col-12 col-md-4">
            <?=$form->field($model, 'place_id')->dropDownList(ArrayHelper::map(Place::find()->all(), 'id', 'name'), ["prompt" => 'Wybierz miejsce']);?>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-4">
            <?=$form->field($model, 'date_start')->textInput(['data-item' => 'datetimepicker', 'autocomplete' => 'off'])?>
        </div>
        <div class="col-12 col-md-4">
            <?=$form->field($model, 'date_end')->textInput(['data-item' => 'datetimepicker', 'autocomplete' => 'off'])?>
        </div>
    </div>
    <div class="form-group">
        <?=Html::submitButton('Save', ['class' => 'btn btn-success'])?>
    </div>
    <?php ActiveForm::end();?>
</div>