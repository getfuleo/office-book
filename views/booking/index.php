<?php

use app\models\Place;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use app\models\Employee;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BookingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rezerwacje';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Dodaj rezerwacje', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 40px;'],
            ],

            [
                'attribute' => 'employee_id',
                'value' =>  function ($data) {
                    
                    if(($employee = $data->getEmployee()->one())){
                        $return  =   $employee->firstname . ' ' .  $employee->lastname . '(' .  $employee->email . ')';
                    }else{
                        $return = "(Brak danych)";
                    }
                    return $return;
                },
                'filter' => ArrayHelper::map(Employee::find()->all(), 'id', function ($data) {

                    return $data['firstname'] . ' ' . $data['lastname'] . '(' . $data['email'] . ')';
                }),
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'prompt' => 'Wybierz'
                ],


            ],

            [
                'attribute' => 'place_id',
                'value' => 'place.name',
                'filter' => ArrayHelper::map(Place::find()->all(), 'id', 'name'),
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'prompt' => 'Wybierz'
                ],

            ],

            //'date_created',
            //'date_updated',
            
            [
                'attribute' => 'date_start',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'type' => 'data',
                    'data-item' => 'datetimepicker', 
                    'autocomplete' => 'off'
                ],
            ],

            [
                'attribute' => 'date_end',
                'filterInputOptions' => [
                    'class' => 'form-control',
                    'type' => 'data',
                    'data-item' => 'datetimepicker', 
                    'autocomplete' => 'off'
                ],
            ],
           
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 140px;'],

            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>