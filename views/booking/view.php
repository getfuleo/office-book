<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Booking */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rezerwacja', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="booking-view">

    <h1><?=Html::encode($this->title)?></h1>

    <p>
    <?=Html::a('&laquo; Wróć do listy', ['index'], ['class' => 'btn btn-light'])?>
        <?=Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
        <?=Html::a('Delete', ['delete', 'id' => $model->id], [
    'class' => 'btn btn-danger',
    'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
    ],
])?>
    </p>

    <?=DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'employee_id',
        'place_id',
        'date_created',
        'date_updated',
        'date_start',
        'date_end',
    ],
])?>

</div>
