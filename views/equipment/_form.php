<?php

use app\models\Place;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Equipment */
/* @var $form yii\widgets\ActiveForm */
?>
<p>
    <?=Html::a('&laquo; Wróć do listy', ['index'], ['class' => 'btn btn-light'])?>
</p>
<div class="equipment-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-12 col-md-4">
            <?= $form->field($model, 'place_id')->dropDownList(ArrayHelper::map(Place::find()->all(), 'id', 'name'), ["prompt" => 'Wybierz miejsce']); ?>
        </div>
        <div class="col-12 col-md-8">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-12 col-md-3">
            <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-12 col-md-3">
            <?= $form->field($model, 'date_buy')->textInput(['type' => 'date', 'maxlength' => true]) ?>
        </div>
        <div class="col-12 col-md-3">
            <?= $form->field($model, 'value')->textInput() ?>
        </div>
        <div class="col-12 col-md-3">
            <?= $form->field($model, 'kind')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <div class="form-group">
        <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>