<?php

use yii\helpers\Url;
use app\models\Place;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Place */

$this->title = 'Edytuj miejsce: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Places', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="place-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<h2>Przypisane wyposażenie</h2>
<hr>
<?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            //  ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 40px;'],
            ],
            [
                'attribute' => 'place_id',
                'value' => 'place.name',
                'filter' => ArrayHelper::map(Place::find()->all(), 'id', 'name'),


            ],

            'kind',
            'model',
            'name',
            //'date_buy',
            //'value',
            //'description:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width: 140px;'],
                'urlCreator' => function( $action, $model, $key, $index ){

                    if ($action == "view") {

                        return Url::to(['equipment/view', 'id' => $key]);

                    }

                    if ($action == "update") {

                        return Url::to(['equipment/update', 'id' => $key]);

                    }

                    if ($action == "delete") {

                        return Url::to(['equipment/delete', 'id' => $key]);

                    }

                }

            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>


</div>
