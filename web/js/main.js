/**
 * DataTime Picker init
 */
$('[data-item="datetimepicker"]').datetimepicker({
    format:'Y-m-d H:i',
    lang:'pl'
});